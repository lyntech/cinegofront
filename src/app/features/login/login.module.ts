import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogLoginComponent } from './dialog-login/dialog-login.component';
import { DialogRegisterComponent } from './dialog-register/dialog-register.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [DialogLoginComponent, DialogRegisterComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    DialogLoginComponent,
    DialogRegisterComponent
  ],
  entryComponents: [
    DialogLoginComponent, DialogRegisterComponent
  ]
})
export class LoginModule { }
