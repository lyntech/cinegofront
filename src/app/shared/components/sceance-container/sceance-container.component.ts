import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Sceance } from 'src/app/models/sceance';
import 'rxjs/add/operator/map';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sceance-container',
  templateUrl: './sceance-container.component.html',
  styleUrls: ['./sceance-container.component.scss']
})
export class SceanceContainerComponent implements OnInit, OnChanges {
  @Input() lessceances: Sceance[];

  // cinemas: Cinema[];
  id: string;
  token;
  constructor(
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    /*this.token = this.cookieService.get('token');
    this.cinemaService.getAllCinemas(this.token).subscribe(data => {
      this.cinemas = data['content'];
    });
    console.log(this.cinemas);*/
  }

  ngOnChanges() {
    if (this.lessceances) {
      console.log('ici');
      console.log(this.lessceances)
    }
  }
}
