import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SignBoard } from 'src/app/models/signBoard';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class SignboardService {

  constructor(private http: HttpClient) { }

  getAllSignBoards(token: string): Observable<SignBoard[]> {
    return this.http.get<SignBoard[]>(`${API_BASE_URL}/enseignes`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  getSignBoardById(id: number, token: string): Observable<SignBoard> {
    return this.http.get<SignBoard>(`${API_BASE_URL}/enseigne/${id}`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  createSignBoard(item: SignBoard, token: string): Observable<any> {
    return this.http.post<SignBoard>(`${API_BASE_URL}/enseigne`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('x-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  updateSignBoard(item: SignBoard, token: string): Observable<any> {
    return this.http.put<SignBoard>(`${API_BASE_URL}/enseigne/${item.id}`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('x-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  deleteSignBoard(id: number, token: string) {
    return this.http.delete(`${API_BASE_URL}/enseigne/${id}`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    // tslint:disable-next-line: forin
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
