import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'movies', loadChildren: './features/movies/movie.module#MovieModule' },
  { path: 'cinemas', loadChildren: './features/cinema/cinema.module#CinemaModule' },
  { path: 'sceances', loadChildren: './features/sceance/sceance.module#SceanceModule' },
  { path: 'signboards', loadChildren: './features/signboard/signboard.module#SignboardModule' },
  { path: 'users', loadChildren: './features/users/user.module#UserModule' },
  { path: 'rooms', loadChildren: './features/rooms/room.module#RoomModule' },
  { path: 'booking/:id', loadChildren: './features/booking/booking.module#BookingModule' },
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
