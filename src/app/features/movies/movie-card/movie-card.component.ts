import { Component, OnInit, Input, AfterContentInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit, AfterContentInit {

  @Input()
  movie: Movie;

  // @ContentChild()
  imageContent: any;

  constructor() { }

  ngOnInit() {

  }

  ngAfterContentInit() {
    console.log('content init', this.imageContent);
  }

  onToggle(event) {
    console.log('highlight toggle event', event);
  }

}
