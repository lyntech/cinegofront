import { CookieService } from 'ngx-cookie-service';
import { UserService } from './../user.service';
import { Component } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { ErrorStateMatcher, MatDialogRef } from '@angular/material';
import { FormControl,FormBuilder, FormGroupDirective, NgForm, Validators, FormGroup,ReactiveFormsModule, ValidatorFn, AbstractControl } from '@angular/forms';
import { passMatching } from './validator';

@Component({
	selector: 'app-dialog-register',
	templateUrl: './user-dialog-resetpass.component.html',
	styleUrls: ['./user-dialog-resetpass.component.scss']
})

export class UserDialogResetPassComponent{
	title = 'app';
	ResetPassGroup: FormGroup;
	constructor(private fb: FormBuilder,
		private userService:UserService,
		private cookieService:CookieService,
		public dialogRef: MatDialogRef<UserDialogResetPassComponent>){
		this.ResetPassGroup = this.fb.group({
		oldpassword: '',
		newPassword1:'',
		newPassword2:['',passMatching]
		});
		this.ResetPassGroup.controls.newPassword1.valueChanges.subscribe(
		x => this.ResetPassGroup.controls.newPassword2.updateValueAndValidity()
		)
	}


	onSubmit(){
		if(this.ResetPassGroup.valid)
		{
			var oldPass = this.ResetPassGroup.root.get("oldpassword").value;
			var newPass = this.ResetPassGroup.root.get("newPassword1").value;
			var id = jwt_decode(this.cookieService.get('token')).id;
			this.userService.changePassUser({oldpass:oldPass,pass:newPass},id,this.cookieService.get('token')).subscribe(data =>  {
				if(data["success"])
				{
					this.closeDialog();
				}
			});
		}
	}

	closeDialog() {
		this.dialogRef.close(UserDialogResetPassComponent.name + ' fermé');
	}
}