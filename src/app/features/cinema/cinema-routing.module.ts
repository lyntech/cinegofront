import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CinemaListComponent } from './cinema-list/cinema-list.component';

const routes: Routes = [
  { path: '', component: CinemaListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CinemaRoutingModule { }
