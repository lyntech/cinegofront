import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { RoomService } from '../room.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Room } from 'src/app/models/room';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs';
import { RoomAddComponent } from '../room-add/room-add.component';
import { RoomEditComponent } from '../room-edit/room-edit.component';
import { SeatMapManagerComponent } from 'src/app/shared/components/seatmap/seatmap-manager/seatmap-manager.component';

export class RoomDataSource extends DataSource<Room> {
  constructor(private roomService: RoomService) {
    super();
  }
  connect(): Observable<Room[]> {
    return this.roomService.getAllRooms();
  }
  disconnect() { }
}

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss']
})
export class RoomListComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'name', 'placelenght', 'cinema', 'techno', 'action'];

  constructor(private roomService: RoomService, public dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.roomService.getAllRooms().subscribe(data => {
      this.dataSource.data = data['content'];
      console.log(data['content']);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openRoomAddDialog() {
    this.dialog.open(RoomAddComponent, {
      width: '450px'
    }).afterClosed().subscribe(() => {
      this.roomService.getAllRooms().subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
  openRoomEditDialog(id: number) {
    this.dialog.open(RoomEditComponent, {
      width: '450px',
      data: {
        dataKey: id
      }
    }).afterClosed().subscribe(() => {
      this.roomService.getAllRooms().subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
  deleteUser(id: number) {
    this.roomService.deleteRoom(id).subscribe(() => {
      console.log('delete ok !');
      this.roomService.getAllRooms().subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }

  openPlanEditDialog(xml: string, id: number) {
    this.dialog.open(SeatMapManagerComponent, {
      width: '700px',
      data: {
        dataKey: xml,
        theid: id
      }
    });
  }
}
