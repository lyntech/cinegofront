import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SignboardService } from '../signboard.service';
import { SignBoard } from 'src/app/models/signBoard';

@Component({
  selector: 'app-signboard-add',
  templateUrl: './signboard-add.component.html',
  styleUrls: ['./signboard-add.component.sass']
})
export class SignboardAddComponent implements OnInit {

  token: string;
  addSignboardForm = new FormGroup({
    name: new FormControl('')
  });

  constructor(
    private cookieService: CookieService,
    private signboardService: SignboardService
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
  }

  onAdd(): void {
    const signboard: SignBoard = this.addSignboardForm.value;
    this.signboardService.createSignBoard(signboard, this.token).subscribe(data => {
      console.log(data);
    });
  }

}
