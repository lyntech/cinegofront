import { Cinema } from 'src/app/models/cinema';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Movie } from 'src/app/models/movie';
import { MovieService } from '../movie.service';
import { MovieCast } from 'src/app/models/movieCast';
import { Sceance } from 'src/app/models/sceance';
import { MatDialog } from '@angular/material';
import { DialogLoginComponent } from '../../login/dialog-login/dialog-login.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  movie: Movie = new Movie();
  movieCasts: MovieCast[];
  id: string;
  sceances: Sceance[];

  constructor(
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private movieService: MovieService,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.movieService.getMovieSceanceById(this.id).subscribe(res => {
      this.movie = res['content']['film'];
      this.sceances = res['content']['seancesbycinema'];
      // console.log(this.sceances);
    });
    this.movieService.getCastsMovieById(this.id).subscribe(data => {
      this.movieCasts = data['content'].slice(0, 6);
    });
  }

  openLoginDialog() {
    if (this.cookieService.check('token')) {
      return;
    } else {
      this.dialog.open(DialogLoginComponent, {
        width: '450px'
      });
    }
  }
}
