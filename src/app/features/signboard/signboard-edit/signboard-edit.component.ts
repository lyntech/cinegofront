import { CookieService } from 'ngx-cookie-service';
import { SignBoard } from 'src/app/models/signBoard';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SignboardService } from '../signboard.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-signboard-edit',
  templateUrl: './signboard-edit.component.html',
  styleUrls: ['./signboard-edit.component.sass']
})
export class SignboardEditComponent implements OnInit {

  signboard: SignBoard = new SignBoard();
  token: string;
  editSignboardForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
  });

  constructor(
    private cookieService: CookieService,
    private signboardService: SignboardService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.signboardService.getSignBoardById(this.data.dataKey, this.token).subscribe(data => {
      this.signboard = data['content'];
      this.editSignboardForm.patchValue({
        id: this.signboard.id,
        name: this.signboard.name
      });
    });
  }

  onEdit() {
    const signboard: SignBoard = this.editSignboardForm.value;
    this.signboardService.updateSignBoard(signboard, this.token).subscribe(data => {
      console.log(data);
    });
  }
}
