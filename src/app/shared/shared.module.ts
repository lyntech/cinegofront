import { SceanceContainerComponent } from './components/sceance-container/sceance-container.component';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SeatmapComponent } from './components/seatmap/seatmap.component';
import { CommonModule } from '@angular/common';
import { CarouselVideoComponent } from './components/carousel-video/carousel-video.component';
import { BypasssecurityPipe } from './pipes/bypasssecurity.pipe';
import { RouterModule } from '@angular/router';
import { SeatMapManagerComponent } from './components/seatmap/seatmap-manager/seatmap-manager.component';

@NgModule({
  declarations: [SeatmapComponent, SceanceContainerComponent, CarouselVideoComponent, BypasssecurityPipe, SeatMapManagerComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  exports: [
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SeatmapComponent,
    SeatMapManagerComponent,
    SceanceContainerComponent,
    CarouselVideoComponent,
  ]
})
export class SharedModule { }
