import { map } from 'rxjs/operators';
import { MovieCardComponent } from './../movie-card/movie-card.component';
import { Component, OnInit, ViewChildren, QueryList, AfterContentInit } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit, AfterContentInit {

  movies: Movie[];

  @ViewChildren(MovieCardComponent)
  moviesRef: QueryList<MovieCardComponent>;

  constructor(
    private movieService: MovieService
  ) { }

  ngOnInit() {
    this.movieService.getAllMovies().subscribe(res => {
      this.movies = res['content'];
      console.log(this.movies);
    });
  }

  ngAfterContentInit(): void {
    console.log('init view children', this.moviesRef);
  }
}
