import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Room } from 'src/app/models/room';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Techno } from 'src/app/models/techno';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private http: HttpClient) { }
  getAllRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(`${API_BASE_URL}/salles`);
  }

  getAllTechnos(): Observable<Techno[]> {
    return this.http.get<Techno[]>(`${API_BASE_URL}/technos`);
  }

  getRoomById(id: number): Observable<Room> {
    return this.http.get<Room>(`${API_BASE_URL}/salle/${id}`);
  }

  createRoom(item: Room): Observable<any> {
    return this.http.post<Room>(`${API_BASE_URL}/salle`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  updateRoom(item: Room): Observable<any> {
    return this.http.put<Room>(`${API_BASE_URL}/salle/${item.id}`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  deleteRoom(id: number) {
    return this.http.delete(`${API_BASE_URL}/salle/${id}`);
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    // tslint:disable-next-line: forin
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
