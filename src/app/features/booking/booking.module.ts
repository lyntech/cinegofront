import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingRoutingModule } from './booking-routing.module';
import { BookingStepperComponent } from './booking-stepper/booking-stepper.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [BookingStepperComponent],
  imports: [
    CommonModule,
    BookingRoutingModule,
    SharedModule
  ]
})
export class BookingModule { }
