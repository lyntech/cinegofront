import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Sceance } from 'src/app/models/sceance';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class SceanceService {
  constructor(private http: HttpClient) { }

  getAllSceances(): Observable<Sceance[]> {
    return this.http.get<Sceance[]>(`${API_BASE_URL}/seances`);
  }

  getSceanceById(id: string): Observable<Sceance> {
    return this.http.get<Sceance>(`${API_BASE_URL}/seance/${id}`);
  }

  createSceance(item: Sceance): Observable<Sceance> {
    return this.http.post<Sceance>(`${API_BASE_URL}/seance`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  updateSceance(item: Sceance): Observable<Sceance> {
    return this.http.put<Sceance>(`${API_BASE_URL}/seance/${item.id}`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  deleteSceance(id: number) {
    return this.http.delete(`${API_BASE_URL}/seance/${id}`);
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    // tslint:disable-next-line: forin
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
