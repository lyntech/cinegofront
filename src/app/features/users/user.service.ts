import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) { }

    getAllUsers(token): Observable<User[]> {
        return this.http.get<User[]>(`${API_BASE_URL}/users`,
        {
            headers: new HttpHeaders()
              .set('x-token', token)
        });
    }

    getUserById(id: number, token): Observable<User> {
        return this.http.get<User>(`${API_BASE_URL}/user/${id}`,
        {
            headers: new HttpHeaders()
              .set('x-token', token)
        });
    }

    createUser(item: User, token): Observable<User> {
        return this.http.post<User>(`${API_BASE_URL}/user`, item,
        {
            headers: new HttpHeaders()
              .set('x-token', token)
        });
    }

    changePassUser(item:{},id:number,token){
        return this.http.post<User>(`${API_BASE_URL}/user/`+ id +'/pass',this.getFormUrlEncoded(item),
        {
            headers:new HttpHeaders()
                .set('x-token',token)
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    updateUser(item: User, token): Observable<User> {
        return this.http.put<User>(`${API_BASE_URL}/user/${item.id}`, item,
        {
            headers: new HttpHeaders()
              .set('x-token', token)
        });
    }

    deleteUser(id: number, token) {
        return this.http.delete(`${API_BASE_URL}/user/${id}`,
        {
            headers: new HttpHeaders()
              .set('x-token', token)
        });
    }
    getFormUrlEncoded(toConvert) {
        const formBody = [];
        // tslint:disable-next-line: forin
        for (const property in toConvert) {
          const encodedKey = encodeURIComponent(property);
          const encodedValue = encodeURIComponent(toConvert[property]);
          formBody.push(encodedKey + '=' + encodedValue);
        }
        return formBody.join('&');
      }
}
