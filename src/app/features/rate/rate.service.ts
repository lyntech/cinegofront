import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Rate } from 'src/app/models/rate';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class RateService {

  constructor(private http: HttpClient) { }

  getAllRates(token): Observable<Rate[]> {
    return this.http.get<Rate[]>(`${API_BASE_URL}/tarifs`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  getAllRatesByIdCinema(id): Observable<any> {
    return this.http.get<any>(`${API_BASE_URL}/tarifs/cinema/${id}`);
  }

  getRateById(id: number): Observable<Rate> {
    return this.http.get<Rate>(`${API_BASE_URL}/tarif/${id}`);
  }

  createRate(item: Rate): Observable<Rate> {
    return this.http.post<Rate>(`${API_BASE_URL}/tarif`, item);
  }

  updateRate(item: Rate): Observable<Rate> {
    return this.http.put<Rate>(`${API_BASE_URL}/tarif/${item.id}`, item);
  }

  deleteRate(id: number) {
    return this.http.delete(`${API_BASE_URL}/tarif/${id}`);
  }
}
