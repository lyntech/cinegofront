import { CinemaService } from './../../cinema/cinema.service';
import { Cinema } from 'src/app/models/cinema';
import { Component, OnInit } from '@angular/core';
import { Room } from 'src/app/models/room';
import { RoomService } from '../room.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Techno } from 'src/app/models/techno';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-room-add',
  templateUrl: './room-add.component.html',
  styleUrls: ['./room-add.component.scss']
})
export class RoomAddComponent implements OnInit {
  cinema: Cinema = new Cinema();
  techno: Techno = new Techno();
  cinemas: Cinema[] = [];
  technos: Techno[] = [];
  token;
  addRoomForm = new FormGroup({
    name: new FormControl(''),
    blueprint: new FormControl(''),
    placelenght: new FormControl(''),
    cinema: new FormControl(''),
    techno: new FormControl(''),
  });

  constructor(
    private roomService: RoomService,
    private cinemaService: CinemaService,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.cinemaService.getAllCinemas(this.token).subscribe(data => {
      this.cinemas = data['content'];
      console.log(this.cinemas);
    });
    this.roomService.getAllTechnos().subscribe(data => {
      this.technos = data['content'];
      console.log(this.technos);
    });
  }

  onAdd(): void {
    const room: Room = this.addRoomForm.value;
    this.roomService.createRoom(room).subscribe(data => {
      console.log(data);
    });
  }

}
