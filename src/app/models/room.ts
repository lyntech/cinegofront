import { Techno } from './techno';
import { Cinema } from './cinema';

export class Room {
    id: number;
    name: string;
    blueprint: string;
    placelenght: number;
    cinema: Cinema;
    techno: Techno;
}
