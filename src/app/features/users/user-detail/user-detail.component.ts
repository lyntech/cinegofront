import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit,ViewChild } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import {Location} from '@angular/common';
import { MatDialog ,MatTableDataSource,MatPaginator, MatSort} from '@angular/material';
import { UserDialogResetPassComponent } from '../user-dialog-resetpass/user-dialog-resetpass';
import { Reservation } from 'src/app/models/reservation';
import { ReservationService } from '../../reservation/reservation.service';
import { DataSource } from '@angular/cdk/table';
import { Observable } from 'rxjs';
// import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {

	reservation:Reservation[];
	constructor(public dialog:MatDialog,
	private reservationService:ReservationService,
	private cookieService:CookieService,
	private unelocation:Location) { }

	account_name = "";
	name = "";
	surname = "";
	dataSource = new MatTableDataSource();
	dataSourceBefore = new MatTableDataSource();
	displayedColumns = ['id', 'date','titreFilm','prixTotal','placeTotal'];//,'action'];
	displayedColumnsBefore = ['id', 'date','titreFilm','prixTotal','placeTotal'];
	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

  	ngOnInit() {
		if (this.cookieService.check('token')) {
			var origin_token = this.cookieService.get('token');
			var token_decode = jwt_decode(origin_token);
			this.account_name = token_decode.login;
			this.name = token_decode.nom;
			this.surname = token_decode.prenom;
			var listeSeancesBefore = [];
			var listeSeancesActu = [];
			this.reservationService.getAllReservationByIdUser(token_decode.id).subscribe(data => {
				for(var reservation of data["content"])
				{
					var prix = 0,nbPlaces = 0;
					for(var place of reservation.places)
					{
						prix+= place.prix;
						nbPlaces++;
					}
					reservation.prixTotal = prix;
					reservation.placeTotal = nbPlaces;
					if(new Date(reservation.sceance.date) < new Date())
					{
						listeSeancesBefore.push(reservation);
					}
					else
					{
						listeSeancesActu.push(reservation);
					}
				}
				this.dataSource.data = listeSeancesActu;
				this.dataSourceBefore.data = listeSeancesBefore;
			});
			
		}
		else
		{
			window.location.href = "http://localhost:4200";
		}
	}

	ngAfterViewInit() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	OpenResetPass()
	{
		this.dialog.open(UserDialogResetPassComponent);
	}
}
export class RoomDataSource extends DataSource<Reservation> {
	constructor(private ReservationService: ReservationService,private cookieService:CookieService) {
	  super();
	}
	connect(): Observable<Reservation[]> {
		return this.ReservationService.getAllReservationByIdUser(jwt_decode(this.cookieService.get('token')).id);
	}
	disconnect() { }
}