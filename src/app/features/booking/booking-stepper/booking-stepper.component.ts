import { User } from './../../../models/user';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { SceanceService } from '../../sceance/sceance.service';
import { Sceance } from 'src/app/models/sceance';
import { RateService } from '../../rate/rate.service';
import { Rate } from 'src/app/models/rate';
import { map } from 'rxjs/operators';
import { SeatmapComponent } from 'src/app/shared/components/seatmap/seatmap.component';
import { CookieService } from 'ngx-cookie-service';
import { ReservationService } from '../../reservation/reservation.service';
import * as jwt_decode from 'jwt-decode';
import { Reservation } from 'src/app/models/reservation';
import { placeReserver } from 'src/app/models/placeReserver';

@Component({
  selector: 'app-booking-stepper',
  templateUrl: './booking-stepper.component.html',
  styleUrls: ['./booking-stepper.component.scss']
})
export class BookingStepperComponent implements OnInit {

  @ViewChild(SeatmapComponent) seatmap;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  rates: Rate[];
  sceance: Sceance;
  id: string;
  token: string;
  idPlaces: Array<number>;
  reservation: Reservation;
  reservedPlaces: placeReserver[] = [];
  tmpPlace;

  rateForm: FormGroup = new FormGroup({
    price: new FormControl(''),
  });
  constructor(
    private route: ActivatedRoute,
    private sceanceService: SceanceService,
    private reservationService: ReservationService,
    private rateService: RateService,
    private formBuilder: FormBuilder,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.token = this.cookieService.get('token');
    this.sceanceService.getSceanceById(this.id).subscribe(data => {
      this.sceance = data['content'];
      let reservation = new Reservation();
      var sceance = data['content']
      reservation.sceance = sceance.id;
      reservation.date = this.getDateNow();
      var user = jwt_decode(this.token);
      reservation.user = user.id;
      this.reservation = reservation;
      this.rateService.getAllRatesByIdCinema(this.sceance.room.cinema.id).pipe(
        map(dat => {
          return dat['content'].filter((rate: Rate) => rate.cinema.id === this.sceance.room.cinema.id);
        })
      ).subscribe(dat => {
        this.rates = dat;
      });
    });
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  getDateNow(): string {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    const dateTime = date + ' ' + time;
    return dateTime;
  }

  getArrayFiltered(array: Array<any>): Array<any> {
    return array.filter(data => data !== null && data !== undefined);
  }

  onNext() {
    this.tmpPlace = this.getArrayFiltered(this.seatmap.selectedPlaceRange);
  }

  onPay(array: Array<number>) {
    this.reservationService.createReservation(this.reservation).subscribe(reserv => {
      this.idPlaces = this.getArrayFiltered(array);
      for (let data of this.idPlaces) {
        this.reservedPlaces.push({
          idPlace: data,
          reservation: reserv.content,
          prix: this.rateForm.value.price,
        });
      }
      for (let reservedPlace of this.reservedPlaces) {
        console.log(reservedPlace);
        this.reservationService.createPlaceReservation(reservedPlace).subscribe(data => {
          console.log(data);
        });
      }
    });
  }
}
