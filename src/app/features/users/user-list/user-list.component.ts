import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { Observable, of as observableof } from 'rxjs';
import { DataSource } from '@angular/cdk/collections';
import { User } from '../../../models/user';
import { UserService } from '../user.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSidenav, MatDialog } from '@angular/material';
import { UserDialogComponent } from '../user-dialog/user-dialog.component';
import { CookieService } from 'ngx-cookie-service';

export class UserDataSource extends DataSource<User> {
  constructor(private userService: UserService, private cookieService: CookieService) {
    super();
  }
  connect(): Observable<User[]> {
    return this.userService.getAllUsers(this.cookieService.get('token'));
  }
  disconnect() { }
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, AfterViewInit {

  displayedColumns = ['id', 'lastname', 'firstname', 'grade', 'action'];
  dataSource = new MatTableDataSource();
  dir: string = 'rtl';

  dataRow: User = new User();

  token;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSidenav) sidenav: MatSidenav;

  constructor(private userService: UserService, public dialog: MatDialog, private cookieService: CookieService) {
  }
  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.userService.getAllUsers(this.token).subscribe(data => {
      console.log(data);
      this.dataSource.data = data['users'];
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  rowClicked(row: any, event: any): void {
    this.sidenav.toggle();
    this.dataRow = row;
  }

  onSubmit() {
    this.userService.updateUser(this.dataRow, this.token).subscribe(data => {
      this.userService.getAllUsers(this.token).subscribe(data => {
        this.dataSource.data = data;
      });
    });
    this.sidenav.toggle();
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id, this.token).subscribe(() => {
      console.log('delete ok !');
      this.userService.getAllUsers(this.token).subscribe(data => {
        this.dataSource.data = data;
      });
    });
  }
  openUserDialog() {
    this.dialog.open(UserDialogComponent, {
      width: '450px'
    }).afterClosed().subscribe(() => {
      this.userService.getAllUsers(this.token).subscribe(data => {
        this.dataSource.data = data;
      });
    });
  }
}
