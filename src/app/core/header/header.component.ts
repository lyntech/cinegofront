import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogLoginComponent } from 'src/app/features/login/dialog-login/dialog-login.component';
import { DialogRegisterComponent } from 'src/app/features/login/dialog-register/dialog-register.component';
import { CookieService } from 'ngx-cookie-service';
import * as jwt_decode from 'jwt-decode';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerTitle = 'Cinego';
  token;
  user: User;
  isLogged = false;
  isAdmin = false;

  constructor(public dialog: MatDialog, private cookieService: CookieService) { }
  ngOnInit() {
    if (this.cookieService.check('token')) {
      this.isLogged = true;
    }
    this.token = this.cookieService.get('token');
    this.user = jwt_decode(this.token);
    if (this.user.rank === 50) {
      this.isAdmin = true;
    }
  }

  openLoginDialog() {
    this.dialog.open(DialogLoginComponent, {
      width: '450px'
    });
  }
  openRegisterDialog() {
    this.dialog.open(DialogRegisterComponent, {
      width: '450px'
    });
  }

  logoff() {
    this.cookieService.delete('token');
    window.location.reload();
  }
}
