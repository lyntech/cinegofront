import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Sceance } from 'src/app/models/sceance';
import { SceanceService } from '../sceance.service';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SceanceEditComponent } from '../sceance-edit/sceance-edit.component';
import { SceanceAddComponent } from '../sceance-add/sceance-add.component';

export class SceanceDataSource extends DataSource<Sceance> {
  constructor(private roomService: SceanceService) {
    super();
  }
  connect(): Observable<Sceance[]> {
    return this.roomService.getAllSceances();
  }
  disconnect() { }
}

@Component({
  selector: 'app-sceance-list',
  templateUrl: './sceance-list.component.html',
  styleUrls: ['./sceance-list.component.scss']
})
export class SceanceListComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'movie', 'date', 'room', 'action'];

  constructor(private sceanceService: SceanceService, public dialog: MatDialog) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.sceanceService.getAllSceances().subscribe(data => {
      this.dataSource.data = data['content'];
      console.log(data['content']);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSceanceAddDialog() {
    this.dialog.open(SceanceAddComponent, {
      width: '450px'
    }).afterClosed().subscribe(() => {
      this.sceanceService.getAllSceances().subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
  openSceanceEditDialog(id: number) {
    this.dialog.open(SceanceEditComponent, {
      width: '450px',
      data: {
        dataKey: id
      }
    }).afterClosed().subscribe(() => {
      this.sceanceService.getAllSceances().subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
}
