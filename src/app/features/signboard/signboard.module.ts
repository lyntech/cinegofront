import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignboardRoutingModule } from './signboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SignboardAddComponent } from './signboard-add/signboard-add.component';
import { SignboardListComponent } from './signboard-list/signboard-list.component';
import { SignboardEditComponent } from './signboard-edit/signboard-edit.component';

@NgModule({
  declarations: [SignboardAddComponent, SignboardListComponent, SignboardEditComponent],
  imports: [
    CommonModule,
    SignboardRoutingModule,
    SharedModule
  ],
  entryComponents: [SignboardAddComponent, SignboardEditComponent]
})
export class SignboardModule { }
