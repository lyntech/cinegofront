import { Component } from '@angular/core';
import { ErrorStateMatcher, MatDialogRef } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { LoginService } from '../login.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-dialog-register',
  templateUrl: './dialog-register.component.html',
  styleUrls: ['./dialog-register.component.scss']
})
export class DialogRegisterComponent {
  matcher = new MyErrorStateMatcher();

  user: User = new User();

  constructor(public dialogRef: MatDialogRef<DialogRegisterComponent>, private loginService: LoginService) { }

  firstNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  lastNameFormControl = new FormControl('', [
    Validators.required,
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl(null, [
    Validators.required,
  ]);
  closeDialog() {
    this.dialogRef.close(DialogRegisterComponent.name + ' fermé');
  }

  register() {
    this.loginService.postRegister(this.user).subscribe((data) => {
      if (data.success) {
        this.dialogRef.close(DialogRegisterComponent.name + ' fermé');
      }
    }, (error) => console.log(error));
    
  }
}
