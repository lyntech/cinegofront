import { Component, OnInit, Inject } from '@angular/core';
import { SceanceService } from '../sceance.service';
import { MovieService } from '../../movies/movie.service';
import { RoomService } from '../../rooms/room.service';
import { Sceance } from 'src/app/models/sceance';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Movie } from 'src/app/models/movie';
import { Room } from 'src/app/models/room';
import { switchMap, tap, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-sceance-edit',
  templateUrl: './sceance-edit.component.html',
  styleUrls: ['./sceance-edit.component.scss']
})
export class SceanceEditComponent implements OnInit {
  editSceanceForm = new FormGroup({
    id: new FormControl(''),
    date: new FormControl(''),
    movie: new FormControl(''),
    room: new FormControl('')
  });
  filteredMovies: Movie[];
  isLoading = false;
  rooms: Room[];
  sceance: Sceance;
  constructor(
    public dialogRef: MatDialogRef<SceanceEditComponent>,
    private sceanceService: SceanceService,
    private movieService: MovieService,
    private roomService: RoomService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.editSceanceForm
      .get('movie')
      .valueChanges
      .pipe(
        tap(() => this.isLoading = true),
        switchMap(value => this.movieService.getMovieByName(value)
          .pipe(
            finalize(() => this.isLoading = false),
          )
        )
      )
      .subscribe(data => this.filteredMovies = data['content']);

    this.roomService.getAllRooms().subscribe(data => {
      this.rooms = data['content'];
    });
    this.sceanceService.getSceanceById(this.data.dataKey).subscribe(data => {
      this.sceance = data['content'];
      this.editSceanceForm.patchValue({
        id: this.sceance.id,
        date: new Date(this.sceance.date).toISOString().slice(0, -1),
        movie: this.sceance.movie.id,
        room: this.sceance.room.id
      });
    });
  }
  onEdit(): void {
    console.log(this.editSceanceForm.get('date').value);
    const sceance: Sceance = this.editSceanceForm.value;
    this.sceanceService.updateSceance(sceance).subscribe(data => {
      console.log(data);
    });
    this.dialogRef.close(SceanceEditComponent.name + " fermé");
  }
}
