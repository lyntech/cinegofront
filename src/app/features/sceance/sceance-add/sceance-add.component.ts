import { MovieService } from './../../movies/movie.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Sceance } from 'src/app/models/sceance';
import { SceanceService } from '../sceance.service';
import { Movie } from 'src/app/models/movie';
import { tap, switchMap, finalize } from 'rxjs/operators';
import { RoomService } from '../../rooms/room.service';
import { Room } from 'src/app/models/room';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-sceance-add',
  templateUrl: './sceance-add.component.html',
  styleUrls: ['./sceance-add.component.scss']
})
export class SceanceAddComponent implements OnInit {
  addSceanceForm = new FormGroup({
    date: new FormControl(''),
    movie: new FormControl(''),
    room: new FormControl('')
  });
  filteredMovies: Movie[];
  isLoading = false;
  rooms: Room[];
  constructor(
    public dialogRef: MatDialogRef<SceanceAddComponent>,
    private sceanceService: SceanceService,
    private movieService: MovieService,
    private roomService: RoomService) { }

  ngOnInit() {
    this.addSceanceForm
      .get('movie')
      .valueChanges
      .pipe(
        tap(() => this.isLoading = true),
        switchMap(value => this.movieService.getMovieByName(value)
          .pipe(
            finalize(() => this.isLoading = false),
          )
        )
      )
      .subscribe(data => this.filteredMovies = data['content']);

    this.roomService.getAllRooms().subscribe(data => {
      this.rooms = data['content'];
    });
  }
  onAdd(): void {
    const sceance: Sceance = this.addSceanceForm.value;
    this.sceanceService.createSceance(sceance).subscribe(data => {
      console.log(data);
    });
    this.dialogRef.close(SceanceAddComponent.name + " fermé");
  }
}
