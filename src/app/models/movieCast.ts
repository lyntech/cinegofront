export class MovieCast {
    // tslint:disable-next-line: variable-name
    cast_id: number;
    character: string;
    id: number;
    name: string;
    // tslint:disable-next-line: variable-name
    profile_path: string;
}