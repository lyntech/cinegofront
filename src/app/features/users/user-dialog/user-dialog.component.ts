import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { UserService } from '../user.service';
import { User } from '../../../models/user';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UserDialogComponent>, private userService: UserService, private cookieService: CookieService) { }
  isChecked = false;
  data: User = new User();
  token;

  ngOnInit() {
    this.token = this.cookieService.get('token');
  }
  closeDialog() {
    this.dialogRef.close();
    console.log(UserDialogComponent.name + ' fermé');
  }
  isAdmin(value) {
    if (value.checked === true) {
      this.data.rank = 1;
    } else {
      this.data.rank = 0;
    }
  }
  onAdd() {
    this.userService.createUser(this.data, this.token).subscribe(data => {
      console.log(data);
    });
    this.closeDialog();
  }
}
