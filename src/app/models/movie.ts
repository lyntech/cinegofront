export class Movie {
    id: number;
    title: string;
    date: Date;
    poster: string;
    overview: string;
}
