import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SceanceListComponent } from './sceance-list/sceance-list.component';

const routes: Routes = [
  { path: '', component: SceanceListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SceanceRoutingModule { }
