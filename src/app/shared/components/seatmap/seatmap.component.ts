import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { SceanceService } from 'src/app/features/sceance/sceance.service';
import { Sceance } from 'src/app/models/sceance';

@Component({
  selector: 'app-seatmap',
  templateUrl: './seatmap.component.html',
  styleUrls: ['./seatmap.component.scss']
})
export class SeatmapComponent implements OnInit {
  id: string;
  sceance: Sceance;
  jsonBlueprint = null;
  selectedPlace: Array<number> = [];
  selectedPlaceRange: Array<string> = [];

  constructor(
    private ngxXml2jsonService: NgxXml2jsonService,
    private route: ActivatedRoute,
    private sceanceService: SceanceService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.sceanceService.getSceanceById(this.id).subscribe(data => {
      this.sceance = data['content'];
      const xmlCleared = this.sceance.room.blueprint.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, '');
      const parser = new DOMParser();
      const xml = parser.parseFromString(xmlCleared, 'text/xml');
      const obj = this.ngxXml2jsonService.xmlToJson(xml);
      this.jsonBlueprint = obj['room'].range;
      console.log(this.jsonBlueprint);
    });
  }
  seatClicked(idPlace, range, type) {
    if (type != 0) {
      if (!this.selectedPlace[idPlace] && !this.selectedPlaceRange[idPlace]) {
        this.selectedPlace[idPlace] = idPlace;
        this.selectedPlaceRange[idPlace] = range + idPlace;
      } else {
        this.selectedPlace[idPlace] = null;
        this.selectedPlaceRange[idPlace] = null;
      }
    }
    console.log(this.selectedPlaceRange);
    this.getSelectSeat();
  }

  getSelectSeat(): number[] {
    console.log(this.selectedPlace);
    return this.selectedPlace;
  }
}
