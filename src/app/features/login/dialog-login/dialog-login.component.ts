import { Component } from "@angular/core";
import { FormControl, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MatDialogRef } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from '../login.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-dialog-login',
  templateUrl: './dialog-login.component.html',
  styleUrls: ['./dialog-login.component.scss']
})
export class DialogLoginComponent {

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
  ]);

  matcher = new MyErrorStateMatcher();
  login: string;
  password: string;

  constructor(
    public dialogRef: MatDialogRef<DialogLoginComponent>,
    private cookieService: CookieService,
    private loginService: LoginService
  ) { }

  closeDialog() {
    this.dialogRef.close(DialogLoginComponent.name + " fermé");
  }

  logIn() {//TODO: only trigger when email format ok
    this.loginService.postLogin(this.login, this.password).subscribe((data) => {
      if (data.success) {
        this.cookieService.set('token', data.token);
        window.location.reload();
      }
    }, (error) => console.log(error));
  }
}
