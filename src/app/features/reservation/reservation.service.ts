import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Reservation } from 'src/app/models/reservation';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { placeReserver } from 'src/app/models/placeReserver';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient) { }

  getAllReservations(token): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(`${API_BASE_URL}/reservations`);
  }

  getReservationById(id: number): Observable<Reservation> {
    return this.http.get<Reservation>(`${API_BASE_URL}/reservation/${id}`);
  }

  getAllReservationByIdUser(id: number): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(`${API_BASE_URL}/reservations/user/${id}`);
  }

  createReservation(item: Reservation): Observable<any> {
    return this.http.post<any>(`${API_BASE_URL}/reservation`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  createPlaceReservation(item: placeReserver): Observable<any> {
    return this.http.post<any>(`${API_BASE_URL}/reservation/place`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  updateReservation(item: Reservation): Observable<Reservation> {
    return this.http.put<Reservation>(`${API_BASE_URL}/reservation/${item.id}`, item);
  }

  deleteReservation(id: number) {
    return this.http.delete(`${API_BASE_URL}/reservation/${id}`);
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    // tslint:disable-next-line: forin
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
