import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  postLogin(login, password): Observable<any> {
    return this.http.post<any>(`${environment.apiBaseUrl}/login`, `login=${login}&password=${password}`, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  postRegister(item: User): Observable<any> {
    return this.http.post<User>(`${environment.apiBaseUrl}/user`, `login=${item.login}&password=${item.password}&nom=${item.nom}&prenom=${item.prenom}`, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }
}