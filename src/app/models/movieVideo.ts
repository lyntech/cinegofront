export class MovieVideo {
    id: string;
    key: string;
    name: string;
    type: string;
}