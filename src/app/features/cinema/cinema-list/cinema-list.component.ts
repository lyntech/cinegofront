import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Cinema } from 'src/app/models/cinema';
import { CinemaService } from '../cinema.service';
import { Observable } from 'rxjs';
import { MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CinemaEditComponent } from '../cinema-edit/cinema-edit.component';
import { CinemaAddComponent } from '../cinema-add/cinema-add.component';
import { CookieService } from 'ngx-cookie-service';

export class CinemaDataSource extends DataSource<Cinema> {

  token: string;

  constructor(
    private cinemaService: CinemaService,
    private cookieService: CookieService
  ) {
    super();
  }
  connect(): Observable<Cinema[]> {
    this.token = this.cookieService.get('token');
    return this.cinemaService.getAllCinemas(this.token);
  }
  disconnect() { }
}

@Component({
  selector: 'app-cinema-list',
  templateUrl: './cinema-list.component.html',
  styleUrls: ['./cinema-list.component.scss']
})
export class CinemaListComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'name', 'adress', 'signBoard', 'action'];
  token: string;

  constructor(
    private cinemaService: CinemaService,
    public dialog: MatDialog,
    private cookieService: CookieService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.cinemaService.getAllCinemas(this.token).subscribe(data => {
      this.dataSource.data = data['content'];
      console.log(data['content']);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openCinemaAddDialog() {
    this.dialog.open(CinemaAddComponent, {
      width: '450px'
    }).afterClosed().subscribe(() => {
      this.cinemaService.getAllCinemas(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }

  openCinemaEditDialog(id: number) {
    this.dialog.open(CinemaEditComponent, {
      width: '450px',
      data: {
        dataKey: id
      }
    }).afterClosed().subscribe(() => {
      this.cinemaService.getAllCinemas(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
  deleteCinema(id: number) {
    this.cinemaService.deleteCinema(id, this.token).subscribe(() => {
      console.log('delete ok !');
      this.cinemaService.getAllCinemas(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
}
