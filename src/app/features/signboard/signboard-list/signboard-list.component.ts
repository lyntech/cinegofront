import { CookieService } from 'ngx-cookie-service';
import { SignboardService } from './../signboard.service';
import { SignboardAddComponent } from './../signboard-add/signboard-add.component';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { SignboardEditComponent } from '../signboard-edit/signboard-edit.component';
import { SignBoard } from 'src/app/models/signBoard';
import { Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/table';

export class RoomDataSource extends DataSource<SignBoard> {

  token: string;

  constructor(
    private signboardService: SignboardService,
    private cookieService: CookieService
  ) {
    super();
  }
  connect(): Observable<SignBoard[]> {
    this.token = this.cookieService.get('token');
    return this.signboardService.getAllSignBoards(this.token);
  }
  disconnect() { }
}

@Component({
  selector: 'app-signboard-list',
  templateUrl: './signboard-list.component.html',
  styleUrls: ['./signboard-list.component.sass']
})
export class SignboardListComponent implements OnInit, AfterViewInit {

  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'name', 'action'];
  token: string;

  constructor(
    public dialog: MatDialog,
    private signboardService: SignboardService,
    private cookieService: CookieService
  ) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.signboardService.getAllSignBoards(this.token).subscribe(data => {
      this.dataSource.data = data['content'];
      console.log(data['content']);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSignBoardAddDialog() {
    this.dialog.open(SignboardAddComponent, {
      width: '450px'
    }).afterClosed().subscribe(() => {
      this.signboardService.getAllSignBoards(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }

  openSignBoardEditDialog(id: number) {
    this.dialog.open(SignboardEditComponent, {
      width: '450px',
      data: {
        dataKey: id
      }
    }).afterClosed().subscribe(() => {
      this.signboardService.getAllSignBoards(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }

  deleteSignBoard(id: number) {
    this.signboardService.deleteSignBoard(id, this.token).subscribe(() => {
      console.log('delete ok !');
      this.signboardService.getAllSignBoards(this.token).subscribe(data => {
        this.dataSource.data = data['content'];
      });
    });
  }
}
