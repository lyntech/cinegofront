import {AbstractControl,FormGroup} from '@angular/forms';

export function passMatching(control: AbstractControl)
{
    if(control && (control.value !== null || control.value !== undefined))
    {
        const newPassword2value = control.value;
        const newPassword1 = control.root.get("newPassword1");
        if(newPassword1)
        {
            const passValue = newPassword1.value;
            if(passValue !== newPassword2value || passValue === '')
            {
                return{
                    isError:true
                };
            }
        }
    }
    return null;
}