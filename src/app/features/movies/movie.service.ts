import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from 'src/app/models/movie';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { MovieVideo } from 'src/app/models/movieVideo';
import { MovieCast } from 'src/app/models/movieCast';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) { }

  getAllMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(`${API_BASE_URL}/films/seances`);
  }

  getMovieById(id: string): Observable<Movie> {
    return this.http.get<Movie>(`${API_BASE_URL}/films/${id}`);
  }

  getMovieSceanceById(id: string): Observable<any> {
    return this.http.get<any>(`${API_BASE_URL}/films/${id}/seances`);
  }

  getMovieByName(filter: string): Observable<Movie[]> {
    // tslint:disable-next-line: triple-equals
    if (filter != '' && filter != null) {
      if (String(filter).indexOf(' ')) {
        filter = String(filter).split(' ').join('');
      }
      return this.http.get<Movie[]>(`${API_BASE_URL}/films/search/${filter}`);
    } else {
      return new Observable<Movie[]>();
    }
  }
  getVideosMovieById(id: string): Observable<MovieVideo> {
    return this.http.get<MovieVideo>(`${API_BASE_URL}/films/${id}/videos`);
  }
  getCastsMovieById(id: string): Observable<MovieCast> {
    return this.http.get<MovieCast>(`${API_BASE_URL}/films/${id}/credits`);
  }
}
