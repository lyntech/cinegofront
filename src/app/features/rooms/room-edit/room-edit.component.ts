import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Cinema } from 'src/app/models/cinema';
import { Techno } from 'src/app/models/techno';
import { RoomService } from '../room.service';
import { CinemaService } from '../../cinema/cinema.service';
import { Room } from 'src/app/models/room';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.scss']
})
export class RoomEditComponent implements OnInit {
  cinema: Cinema = new Cinema();
  techno: Techno = new Techno();
  cinemas: Cinema[];
  technos: Techno[];
  room: Room = new Room();
  token;
  editRoomForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    placelenght: new FormControl(''),
    cinema: new FormControl(''),
    techno: new FormControl('')
  });
  constructor(
    private roomService: RoomService,
    private cinemaService: CinemaService,
    private cookieService: CookieService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.cinemaService.getAllCinemas(this.token).subscribe(data => {
      this.cinemas = data['content'];
    });
    this.roomService.getAllTechnos().subscribe(data => {
      this.technos = data['content'];
    });
    this.roomService.getRoomById(this.data.dataKey).subscribe(data => {
      this.room = data['content'];
      this.editRoomForm.patchValue({
        id: this.room.id,
        name: this.room.name,
        placelenght: this.room.placelenght,
        cinema: this.room.cinema.id,
        techno: this.room.techno.id
      });
    });
  }

  onEdit() {
    console.log(this.editRoomForm.value);
    const room: Room = this.editRoomForm.value;
    this.roomService.updateRoom(room).subscribe(data => {
      console.log(data);
    });
  }

}
