export class User {
    id: number;
    login: string;
    prenom: string;
    nom: string;
    rank: number;
    password: string;
}
