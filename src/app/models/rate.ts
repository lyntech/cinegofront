import { Cinema } from './cinema';

export class Rate {
    id: number;
    name: string;
    price: number;
    cinema: Cinema;
}