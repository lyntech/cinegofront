import { User } from './user';
import { Sceance } from './sceance';
import { placeReserver } from './placeReserver';

export class Reservation{
    id:number;
    user:User;
    date:string;
    sceance:Sceance;
    places:placeReserver;
}