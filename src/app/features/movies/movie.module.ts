import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieRoutingModule } from './movie-routing.module';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MovieService } from './movie.service';

@NgModule({
  declarations: [
    MovieListComponent,
    MovieDetailComponent,
    MovieCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MovieRoutingModule
  ],
  exports: [
    MovieListComponent
  ],
  providers: [MovieService]
})
export class MovieModule { }
