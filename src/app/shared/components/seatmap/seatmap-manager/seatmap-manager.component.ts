import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-seatmap-manager',
  templateUrl: './seatmap-manager.component.html',
  styleUrls: ['./seatmap-manager.component.scss']
})
export class SeatMapManagerComponent implements OnInit {
  jsonBlueprint = null;
  selectedItem: Array<boolean> = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: HttpClient,
    private ngxXml2jsonService: NgxXml2jsonService,
    private render: Renderer2
  ) {
    var xmlCleared = this.data.dataKey.replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, '');
    const parser = new DOMParser();
    const xml = parser.parseFromString(xmlCleared, 'text/xml');
    const obj = this.ngxXml2jsonService.xmlToJson(xml);
    this.jsonBlueprint = obj['room'].range;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    const classArr: any = document.querySelectorAll('.seat');
    classArr.forEach(element => {
      this.render.listen(element, 'click', (e) => {

        var type = element.getAttribute("data-type");

        if (type == 0) {
          e.target.outerHTML = "<mat-icon>event_seat</mat-icon>";
          element.setAttribute("data-type", 1);
        }
        else if (type == 1) {
          e.target.innerHTML = "<mat-icon>accessible</mat-icon>";
          element.setAttribute("data-type", 2);
        }
        else if (type == 2) {
          e.target.outerHTML = '<div class="couloir-placeholder"></div>';
          element.setAttribute("data-type", 0);
        }
      })
    });
  }

  confirmBlueprintEdit() {
    // var xmlcontainer = document.createElement("div");
    // xmlcontainer.appendChild(document.createTextNode('<?xml version="1.0" encoding="UTF-8"?>'));
    var xml = '<?xml version="1.0" encoding="UTF-8"?><room>';
    const classArr: any = document.querySelectorAll('.blueprint-ligne');
    classArr.forEach(function (element, index) {
      xml += '<range id="' + index + '" size="' + element.childNodes.length + '">';
      for (var i = 2; i < element.childNodes.length; i++) {
        xml += '<place id="' + (i - 1) + '" type="' + element.childNodes[i].attributes[2].value + '" location="' + (i - 1) + '"/>';
      }
      xml += '</range>';
    });
    xml += '</room>';
    console.log(xml)

    this.http.put<any>(`${environment.apiBaseUrl}/salle/${this.data.theid}/blueprint`,
      `blueprint=${xml}`,
      { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
      .subscribe((data) => {
        if (data.success) {
          window.location.reload();
        }
      },
        (error) => console.log(error));
  }
}
