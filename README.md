<h1 align="center"> 🎬 FRONT : Projet CINEGO 👋</h1>

Plateforme de réservation de place de cinéma permettante de :

* Visualiser la description détaillé d'un film
* Réserver et payer une place de cinéma
* Administrer le planing horaire d'un film pour sa visualisation en cinéma
* Ajouter des films en favoris

## Project setup
```sh
npm install
```

And remove `.example` from `src/environnements/environnement.ts.example` file.

### Compiles and hot-reloads for development
```sh
npm run serve
```

### Compiles and minifies for production
```sh
npm run build
```

### Run your unit tests
```sh
npm run test:unit
```

## Packages utilisés

- Angular
- Angular Material
- Typescript
- Axios
- jwt-decode
- Bootstrap (pour la mise en forme)