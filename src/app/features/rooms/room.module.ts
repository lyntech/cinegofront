import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomRoutingModule } from './room-routing.module';
import { RoomListComponent } from './room-list/room-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RoomService } from './room.service';
import { RoomAddComponent } from './room-add/room-add.component';
import { RoomEditComponent } from './room-edit/room-edit.component';
import { MatIconModule } from '@angular/material/icon';
import { SeatMapManagerComponent } from 'src/app/shared/components/seatmap/seatmap-manager/seatmap-manager.component';

@NgModule({
  declarations: [RoomListComponent, RoomAddComponent, RoomEditComponent],
  imports: [
    CommonModule,
    RoomRoutingModule,
    SharedModule,
    MatIconModule
  ],
  providers: [
    RoomService
  ],
  entryComponents: [
    RoomAddComponent, RoomEditComponent, SeatMapManagerComponent
  ]
})
export class RoomModule { }
