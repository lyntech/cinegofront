import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SceanceRoutingModule } from './sceance-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SceanceService } from './sceance.service';
import { SceanceListComponent } from './sceance-list/sceance-list.component';
import { SceanceAddComponent } from './sceance-add/sceance-add.component';
import { SceanceEditComponent } from './sceance-edit/sceance-edit.component';
@NgModule({
  declarations: [
    SceanceListComponent,
    SceanceAddComponent,
    SceanceEditComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SceanceRoutingModule
  ],
  providers: [
    SceanceService
  ],
  entryComponents: [
    SceanceAddComponent, SceanceEditComponent
  ]
})
export class SceanceModule { }
