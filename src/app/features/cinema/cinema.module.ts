import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CinemaRoutingModule } from './cinema-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CinemaListComponent } from './cinema-list/cinema-list.component';
import { CinemaService } from './cinema.service';
import { CinemaAddComponent } from './cinema-add/cinema-add.component';
import { CinemaEditComponent } from './cinema-edit/cinema-edit.component';

@NgModule({
  declarations: [CinemaListComponent, CinemaAddComponent, CinemaEditComponent],
  imports: [
    CommonModule,
    CinemaRoutingModule,
    SharedModule
  ],
  providers: [CinemaService],
  entryComponents: [CinemaAddComponent, CinemaEditComponent]
})
export class CinemaModule { }
