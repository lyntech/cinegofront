import { CookieService } from 'ngx-cookie-service';
import { SignboardService } from './../../signboard/signboard.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Cinema } from 'src/app/models/cinema';
import { SignBoard } from 'src/app/models/signBoard';
import { CinemaService } from '../cinema.service';

@Component({
  selector: 'app-cinema-add',
  templateUrl: './cinema-add.component.html',
  styleUrls: ['./cinema-add.component.sass']
})
export class CinemaAddComponent implements OnInit {

  signboard: SignBoard = new SignBoard();
  signboards: SignBoard[] = [];
  token;
  addCinemaForm = new FormGroup({
    name: new FormControl(''),
    adress: new FormControl(''),
    signboard: new FormControl(''),
  });

  constructor(
    private cinemaService: CinemaService,
    private signboardService: SignboardService,
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.signboardService.getAllSignBoards(this.token).subscribe(data => {
      this.signboards = data['content'];
      console.log(this.signboards);
    });
  }

  onAdd(): void {
    const cinema: Cinema = this.addCinemaForm.value;
    this.cinemaService.createCinema(cinema, this.token).subscribe(data => {
      console.log(data);
    });
  }
}
