import { SignboardService } from './../../signboard/signboard.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SignBoard } from 'src/app/models/signBoard';
import { Cinema } from 'src/app/models/cinema';
import { CinemaService } from '../cinema.service';
import { CookieService } from 'ngx-cookie-service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-cinema-edit',
  templateUrl: './cinema-edit.component.html',
  styleUrls: ['./cinema-edit.component.sass']
})
export class CinemaEditComponent implements OnInit {

  signboard: SignBoard = new SignBoard();
  signboards: SignBoard[];
  cinema: Cinema = new Cinema();
  token;
  editCinemaForm: FormGroup = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    adress: new FormControl(''),
    signboard: new FormControl(''),
  });

  constructor(
    private cinemaService: CinemaService,
    private signboardService: SignboardService,
    private cookieService: CookieService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.token = this.cookieService.get('token');
    this.signboardService.getAllSignBoards(this.token).subscribe(data => {
      this.signboards = data['content'];
    });
    this.cinemaService.getCinemaById(this.data.dataKey, this.token).subscribe(data => {
      this.cinema = data['content'];
      this.editCinemaForm.patchValue({
        id: this.cinema.id,
        name: this.cinema.name,
        adress: this.cinema.adress,
        signboard: this.cinema.signboard.id,
      });
    });
  }

  onEdit() {
    console.log(this.editCinemaForm.value);
    const cinema: Cinema = this.editCinemaForm.value;
    this.cinemaService.updateCinema(cinema, this.token).subscribe(data => {
      console.log(data);
    });
  }
}
