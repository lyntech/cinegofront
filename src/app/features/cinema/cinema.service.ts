import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cinema } from 'src/app/models/cinema';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_BASE_URL: string = environment.apiBaseUrl;

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  constructor(private http: HttpClient) { }
  getAllCinemas(token): Observable<Cinema[]> {
    return this.http.get<Cinema[]>(`${API_BASE_URL}/cinemas`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  getCinemaById(id: number, token: string): Observable<Cinema> {
    return this.http.get<Cinema>(`${API_BASE_URL}/cinema/${id}`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  createCinema(item: Cinema, token: string): Observable<Cinema> {
    return this.http.post<Cinema>(`${API_BASE_URL}/cinema`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('x-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  updateCinema(item: Cinema, token: string): Observable<Cinema> {
    return this.http.put<Cinema>(`${API_BASE_URL}/cinema/${item.id}`, this.getFormUrlEncoded(item), {
      headers: new HttpHeaders()
        .set('x-token', token)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  deleteCinema(id: number, token: string) {
    return this.http.delete(`${API_BASE_URL}/cinema/${id}`, {
      headers: new HttpHeaders()
        .set('x-token', token)
    });
  }

  getFormUrlEncoded(toConvert) {
    const formBody = [];
    // tslint:disable-next-line: forin
    for (const property in toConvert) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(toConvert[property]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    return formBody.join('&');
  }
}
