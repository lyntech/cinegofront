import { SignBoard } from './signBoard';

export class Cinema {
    id: number;
    name: string;
    adress: string;
    signboard: SignBoard;
}
