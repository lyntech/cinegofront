import { LoginModule } from './../features/login/login.module';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { HeaderComponent } from './header/header.component';
import { EnsureModuleLoadedOnceGuard } from './ensure-module-loaded-once.guard';
import { MaterialModule } from '../shared/material.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    MaterialModule,
    LoginModule
  ],
  exports: [
    RouterModule,
    HttpClientModule,
    HeaderComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    super(parentModule);
  }
}
