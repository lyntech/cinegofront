import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MovieVideo } from 'src/app/models/movieVideo';
import { MovieService } from 'src/app/features/movies/movie.service';
import { ActivatedRoute } from '@angular/router';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  selector: 'app-carousel-video',
  templateUrl: './carousel-video.component.html',
  styleUrls: ['./carousel-video.component.scss']
})
export class CarouselVideoComponent implements OnInit {

  movieVideos: MovieVideo[];
  id: string;

  constructor(private movieService: MovieService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.movieService.getVideosMovieById(this.id).subscribe(data => {
      this.movieVideos = data['content'].filter((movieVideo: MovieVideo) => movieVideo.type === 'Trailer');
      console.log(this.movieVideos);
    });
  }

}
