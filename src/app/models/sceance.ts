import { Movie } from 'src/app/models/movie';
import { Room } from './room';

export class Sceance {
    id: number;
    date: Date;
    movie: Movie;
    room: Room;
}
