import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserService } from './user.service';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { UserDialogResetPassComponent } from './user-dialog-resetpass/user-dialog-resetpass';

@NgModule({
  declarations: [UserRoutingModule.components, UserDialogComponent,UserDialogResetPassComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ],
  exports: [
    UserDetailComponent
  ],
  providers: [
    UserService
  ],
  entryComponents: [
    UserDialogComponent,UserDialogResetPassComponent
  ]
})
export class UserModule { }
